# SlovenskoIT

## Preparations

Use with Ubuntu 22.04 LTS

Install prerequisites:

```
sudo apt update
sudo apt install python3-pip git 
```

After install ansible with:
```
pip install ansible
```

These rulebooks use docker so you need to install docker plugin for ansible as well with this command:
```
ansible-galaxy collection install community.docker
```

Make sure that SSH is installed on the server.
After that generate private/public keys with
```
ssh-keygen
```

After you have your keys generated then copy the public key to managed server with command
```
ssh-copy-id username@server
```
where username is the username of a user that has access to sudo and will be used to manage the server and server will be IP adress of the server that will be managed.

## Running

Run by doing the following:

```
git clone https://gitlab.com/slendog/slovenskoit.git
cd slovenskoit/task1
```

Edit file ansible.cfg and change **remote_user** to username that will be used to manage the server and change the path to the private key if you generated it to a custom location instead of default one.

Add ip adresses of servers you will be managing into hosts file.

You will be able to run by using the following command:
```
ansible-playbook --ask-become-pass start.yml --extra-vars "pass=password web_server=web_server_name image=image_name p=80"
```
## Variables

You can change the password of user deploy by changing pass variable in
```
pass=password
```
You can change the name of the docker container by changing web_server variable in 
```
web_server=web_server_name
```

You can change the name of the docker image by changing image variable.
For example:
```
image=image_name
```

You can change he port of the web server by changing the p variable.
For example:
```
p=80
```
